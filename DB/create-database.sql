Create Database StudentManagementAppDB
GO
USE StudentManagementAppDB;
GO 
CREATE Table Student(
    Id INT IDENTITY (1,1),
    Name VARCHAR(50),
    LastName VARCHAR(50),
    CountryId INT,
    CourseId INT,
    CourseAssigned BIT,
    Email VARCHAR(50),
    BirthDay DATETIME,
    ImageUrl VARCHAR(150),
)
GO
CREATE Table Country(
    Id INT IDENTITY (1,1),
    Name VARCHAR(50),
)
GO 
INSERT INTO Country (Name) VALUES ('Argentina');
INSERT INTO Country (Name) VALUES ('Bolivia');
INSERT INTO Country (Name) VALUES ('Chile');
INSERT INTO Country (Name) VALUES ('Colombia');
INSERT INTO Country (Name) VALUES ('Costa Rica');
INSERT INTO Country (Name) VALUES ('Dominican Republic');
INSERT INTO Country (Name) VALUES ('Ecuador');
INSERT INTO Country (Name) VALUES ('El Salvador');
INSERT INTO Country (Name) VALUES ('Guatemala');
INSERT INTO Country (Name) VALUES ('Guyana');
INSERT INTO Country (Name) VALUES ('Honduras');
INSERT INTO Country (Name) VALUES ('Jamaica');
INSERT INTO Country (Name) VALUES ('Mexico');
INSERT INTO Country (Name) VALUES ('Panama');
INSERT INTO Country (Name) VALUES ('Paraguay');
INSERT INTO Country (Name) VALUES ('Peru');
INSERT INTO Country (Name) VALUES ('Suriname');